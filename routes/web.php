<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// recipes CRUD
$router->group(['prefix' => 'v1/recipes'], function($router) {
    $router->get('view-all', 'RecipeController@index');
    $router->get('view/{id}', 'RecipeController@view');
    $router->get('view-medicine-list/{id}', 'RecipeController@viewMedList');
    $router->post('create', 'RecipeController@create');
    $router->post('update/{id}', 'RecipeController@update');
    $router->post('delete/{id}', 'RecipeController@delete');
});

// recipes only CRUD
$router->group(['prefix' => 'v1/recipes-only'], function($router) {
    $router->get('view-all', 'RecipeOnlyController@index');
    $router->get('view/{id}', 'RecipeOnlyController@view');
    $router->post('create', 'RecipeOnlyController@create');
    $router->post('update/{id}', 'RecipeOnlyController@update');
    $router->post('delete/{id}', 'RecipeOnlyController@delete');
});

// medicine list CRUD
$router->group(['prefix' => 'v1/medicine-list'], function($router) {
    $router->get('view-all', 'MedicineListController@index');
    $router->get('view/{id}', 'MedicineListController@view');
    $router->post('create', 'MedicineListController@create');
    $router->post('update/{id}', 'MedicineListController@update');
    $router->post('delete/{id}', 'MedicineListController@delete');
});

// medicines CRUD
$router->group(['prefix' => 'v1/medicines'], function($router) {
    $router->get('view-all', 'MedicineController@index');
    $router->get('view/{id}', 'MedicineController@view');
    $router->post('create', 'MedicineController@create');
    $router->post('update/{id}', 'MedicineController@update');
    $router->post('delete/{id}', 'MedicineController@delete');
});

// doses CRUD
$router->group(['prefix' => 'v1/doses'], function($router) {
    $router->get('view-all', 'DoseController@index');
    $router->get('view/{id}', 'DoseController@view');
    $router->post('create', 'DoseController@create');
    $router->post('update/{id}', 'DoseController@update');
    $router->post('delete/{id}', 'DoseController@delete');
});

// categories CRUD
$router->group(['prefix' => 'v1/categories'], function($router) {
    $router->get('view-all', 'CategoryController@index');
    $router->get('view/{id}', 'CategoryController@view');
    $router->post('create', 'CategoryController@create');
    $router->post('update/{id}', 'CategoryController@update');
    $router->post('delete/{id}', 'CategoryController@delete');
});

// users CRUD
$router->group(['prefix' => 'v1/users'], function($router) {
    $router->get('view-all', 'UserController@index');
    $router->get('view/{id}', 'UserController@view');
    $router->post('create', 'UserController@create');
    $router->post('update/{id}', 'UserController@update');
    $router->post('delete/{id}', 'UserController@delete');
});

// doctors CRUD
$router->group(['prefix' => 'v1/doctors'], function($router) {
    $router->get('view-all', 'DoctorController@index');
    $router->get('view/{id}', 'DoctorController@view');
    $router->post('create', 'DoctorController@create');
    $router->post('update/{id}', 'DoctorController@update');
    $router->post('delete/{id}', 'DoctorController@delete');
});

// patients CRUD
$router->group(['prefix' => 'v1/patients'], function($router) {
    $router->get('view-all', 'PatientController@index');
    $router->get('view/{id}', 'PatientController@view');
    $router->post('create', 'PatientController@create');
    $router->post('update/{id}', 'PatientController@update');
    $router->post('delete/{id}', 'PatientController@delete');
});

// pharmacists CRUD
$router->group(['prefix' => 'v1/pharmacists'], function($router) {
    $router->get('view-all', 'PharmacistController@index');
    $router->get('view/{id}', 'PharmacistController@view');
    $router->post('create', 'PharmacistController@create');
    $router->post('update/{id}', 'PharmacistController@update');
    $router->post('delete/{id}', 'PharmacistController@delete');
});

// alarms CRUD
$router->group(['prefix' => 'v1/alarms'], function($router) {
    $router->get('view-all', 'AlarmController@index');
    $router->get('view/{id}', 'AlarmController@view');
    $router->post('create', 'AlarmController@create');
    $router->post('update/{id}', 'AlarmController@update');
    $router->post('delete/{id}', 'AlarmController@delete');
});