<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDoses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doses', function(Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->integer('min_dose')->nullable();
            $table->integer('max_dose')->nullable();
            $table->string('unit')->nullable();
            $table->integer('min_age')->unsigned();
            $table->integer('max_age')->unsigned();
            $table->integer('time_1');
            $table->string('time_unit_1')->nullable();
            $table->integer('time_2');
            $table->string('time_unit_2')->nullable();
            $table->integer('category_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
