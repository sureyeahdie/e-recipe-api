<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMedicineList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicine_lists', function(Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('name')->nullable();
            $table->string('doses')->nullable();
            $table->string('status')->nullable();
            $table->string('description')->nullable();
            $table->integer('quanitity')->unsigned();
            $table->integer('medicine_id')->nullable();
            $table->integer('recipe_id')->nullable();
            $table->integer('alarm_1');
            $table->integer('alarm_2');
            $table->integer('alarm_3');
            $table->string('alarm_desc_1')->nullable();
            $table->string('alarm_desc_2')->nullable();
            $table->string('alarm_desc_3')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
