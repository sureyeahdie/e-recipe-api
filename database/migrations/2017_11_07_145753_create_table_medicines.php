<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMedicines extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicines', function(Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('name')->nullable();
            $table->string('generic_name')->nullable();
            $table->string('summary')->nullable();
            $table->string('description')->nullable();
            $table->string('side_effect')->nullable();
            $table->string('image')->nullable();
            $table->integer('price')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->integer('amount')->unsigned();
            $table->string('unit')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
