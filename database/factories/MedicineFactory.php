<?php

use App\Medicinee;

$factory->define(App\Medicine::class, function (Faker\Generator $faker) {
    return [
        'name' => null,
        'generic_name' => null,
        'summary' => null,
        'description' => null,
        'side_effect' => null,
        'image' => null,
        'price' => null,
        'category_id' => null,
        'amount' => null,
        'unit' => null,
    ];
});
