<?php

use Illuminate\Database\Seeder;

class MedicineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('medicines')->insert([
            'name' => 'A & D',
            'generic_name' => 'vitamins A, D, and E (topical) (VYE ta mins A, D, and E TOP i kal)',
            'summary' => 
                'Vitamins A, D, and E topical (for the skin) is a skin protectant. It works by moisturizing and sealing the skin, and aids in skin healing.
                
                This medication is used to treat diaper rash, dry or chafed skin, and minor cuts or burns.
                
                Vitamins A, D, and E may also be used for purposes not listed in this medication guide.',
            'side_effect' => 
                'Applies to vitamins a, d, and e topical: topical cream, topical lotion, topical ointment
                
                Get emergency medical help if you have any of these signs of an allergic reaction while taking vitamins a, d, and e topical: hives; difficult breathing; swelling of your face, lips, tongue, or throat.
                
                Stop using the medication and call your doctor at once if your child has a serious side effect such as warmth, redness, oozing, or severe irritation where the medicine is applied.
                
                
                This is not a complete list of side effects and others may occur. Call your doctor for medical advice about side effects.
                
                Some side effects of vitamins a, d, and e topical may not be reported. Always consult your doctor or healthcare specialist for medical advice. You may also report side effects to the FDA.'
        ]);

        DB::table('medicines')->insert([
            'name' => 'B & O',
            'generic_name' => 'Opium',
            'summary' => 
                'Opium preparation is an opioid. An opioid is sometimes called a narcotic.
                
                Opium is derived from the seed pod of a poppy plant. It works by increasing smooth muscle tone and decreasing fluid secretions in the intestines. This slows the movement of bowel matter through the intestines.
                
                Opium preparation (sometimes called "opium tincture") is used to treat diarrhea.
                
                Opium preparation may also be used for purposes not listed in this medication guide.',
            'side_effect' => 
                'Applies to opium: oral tincture
                
                Along with its needed effects, opium may cause some unwanted effects. Although not all of these side effects may occur, if they do occur they may need medical attention.
                
                Check with your doctor immediately if any of the following side effects occur while taking opium:
                
                Incidence not known
                Difficulty having a bowel movement (stool)
                hives or welts
                nausea
                vomiting'
        ]);

        DB::table('medicines')->insert([
            'name' => 'C-500 (Oral)',
            'generic_name' => 'ascorbic acid (Oral route)',
            'summary' => 
                'Vitamins A, D, and E topical (for the skin) is a skin protectant. It works by moisturizing and sealing the skin, and aids in skin healing.
                
                This medication is used to treat diaper rash, dry or chafed skin, and minor cuts or burns.
                
                Vitamins A, D, and E may also be used for purposes not listed in this medication guide.',
            'side_effect' => 
                'Applies to vitamins a, d, and e topical: topical cream, topical lotion, topical ointment
                
                Get emergency medical help if you have any of these signs of an allergic reaction while taking vitamins a, d, and e topical: hives; difficult breathing; swelling of your face, lips, tongue, or throat.
                
                Stop using the medication and call your doctor at once if your child has a serious side effect such as warmth, redness, oozing, or severe irritation where the medicine is applied.
                
                
                This is not a complete list of side effects and others may occur. Call your doctor for medical advice about side effects.
                
                Some side effects of vitamins a, d, and e topical may not be reported. Always consult your doctor or healthcare specialist for medical advice. You may also report side effects to the FDA.'
        ]);
    }
}
