<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MedicineList extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'doses', 'status', 'description', 'quantity',
        'alarm_1', 'alarm_2', 'alarm_3',
        'alarm_desc_1', 'alarm_desc_2', 'alarm_desc_3',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    protected $dates = ['deleted_at'];

    public function recipe() {
        return $this->belongsTo(Recipe::class, 'recipe_id');
    }
}
