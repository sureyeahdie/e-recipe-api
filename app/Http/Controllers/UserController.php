<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;

class UserController extends Controller
{
    // return all items found
    public function index(Request $request) {
        $user = User::all();
        
        if(count($user) == 0) {
            $response = [
                'success' => false,
                'messages' => 'no data found',
            ];
        } elseif(count($user) == 1) {
            $response = [
                'success' => true,
                'messages' => count($user) . ' data found',
                'data' => $user,
            ];
        } else {
            $response = [
                'success' => true,
                'messages' => count($user) . ' datas found',
                'data' => $user,
            ];
        }
        
        return response()->json($response);
    }
    // view item by id
    public function view(Request $request, $id) {
        $user = User::find($id);

        if($user) {
            $response = [
                'success' => true,
                'messages' => 'Data found!',
                'data' => $user,
            ];
        } else {
            $response = [
                'success' => false,
                'messages' => 'Data not found!',
            ];
        }

        return response()->json($response);
    }
    // create new item
    public function create(Request $request) {
        $r = $request->all();
        
        DB::beginTransaction();
        try {
            $user = new User;
            $user->first_name = isset($r['first_name']) ? $r['first_name'] : null;
            $user->last_name = isset($r['last_name']) ? $r['last_name'] : null;
            $user->gender = isset($r['gender']) ? $r['gender'] : 'Male';
            $user->birth_date = isset($r['birth_date']) ? $r['birth_date'] : null;
            $user->address = isset($r['address']) ? $r['address'] : null;
            $user->phone_number = isset($r['phone_number']) ? $r['phone_number'] : null;
            $user->email = isset($r['email']) ? $r['email'] : null;
            $user->password = isset($r['password']) ? $r['password'] : null;
            $user->role = isset($r['role']) ? $r['role'] : null;
            $user->description = isset($r['description']) ? $r['description'] : null;
            $user->save();

            DB::commit();
            $response = [
                'success' => true,
                'messages' => 'Success!',
                'data' => $user,
            ];
        } catch(QueryException $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Failed to create',
            ];
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Oops! Something went wrong.',
            ];
        }
        return response()->json($response);
    }
    // update item by id
    public function update(Request $request, $id) {
        $r = $request->all();

        DB::beginTransaction();
        try {
            $user = User::find($id);
            if($user) {
                $user->name = isset($r['name']) ? $r['name'] : $user->name;
                $user->email = isset($r['email']) ? $r['email'] : $user->email;
                $user->phone_number = isset($r['phone_number']) ? $r['phone_number'] : $user->phone_number;
                $user->specialist = isset($r['specialist']) ? $r['specialist'] : $user->specialist;
                $user->save();
    
                DB::commit();
                $response = [
                    'success' => true,
                    'messages' => 'Success!',
                    'data' => $user,
                ];
            } else {
                DB::rollBack();
                $response = [
                    'success' => false,
                    'messages' => 'Data not found!!',
                ];
            }
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Failed to create!',
            ];
        }
        return response()->json($response);
    }
    // delete id
    public function delete(Request $request, $id) {
        DB::beginTransaction();
        try {
            $user = User::find($id);
            if($user) {
                $user->delete();
                DB::commit();
                $response = [
                    'success' => true,
                    'message' => 'Success deleted data!',
                ];
            } else {
                DB::rollBack();
                $response = [
                    'success' => false,
                    'message' => 'Data not found!',
                ];
            }
            
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'message' => 'Failed to delete!',
            ];
        }
        return response()->json($response);
    }
}