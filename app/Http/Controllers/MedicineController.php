<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Medicine;
use DB;

class MedicineController extends Controller
{
    // return all items found
    public function index(Request $request) {
        $medicine = Medicine::all();
        
        if(count($medicine) == 0) {
            $response = [
                'success' => false,
                'messages' => 'no data found',
            ];
        } elseif(count($medicine) == 1) {
            $response = [
                'success' => true,
                'messages' => count($medicine) . ' data found',
                'data' => $medicine,
            ];
        } else {
            $response = [
                'success' => true,
                'messages' => count($medicine) . ' datas found',
                'data' => $medicine,
            ];
        }
        
        return response()->json($response);
    }
    // view item by id
    public function view(Request $request, $id) {
        $medicine = Medicine::find($id);

        if($medicine) {
            $response = [
                'success' => true,
                'messages' => 'Data found!',
                'data' => $medicine,
            ];
        } else {
            $response = [
                'success' => false,
                'messages' => 'Data not found!',
            ];
        }

        return response()->json($response);
    }
    // create new item
    public function create(Request $request) {
        $r = $request->all();
        
        DB::beginTransaction();
        try {
            $medicine = new Medicine;
            $medicine->name = isset($r['name']) ? $r['name'] : null;
            $medicine->generic_name = isset($r['generic_name']) ? $r['generic_name'] : null;
            $medicine->summary = isset($r['summary']) ? $r['summary'] : null;
            $medicine->description = isset($r['description']) ? $r['description'] : null;
            $medicine->price = isset($r['price']) ? $r['price'] : null;
            $medicine->category_id = isset($r['category_id']) ? $r['category_id'] : null;
            $medicine->amount = isset($r['amount']) ? $r['amount'] : null;
            $medicine->unit = isset($r['unit']) ? $r['unit'] : null;
            $medicine->save();

            DB::commit();
            $response = [
                'success' => true,
                'messages' => 'Success!',
                'data' => $medicine,
            ];
        } catch(QueryException $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Failed to create',
            ];
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Oops! Something went wrong.',
            ];
        }
        return response()->json($response);
    }
    // update item by id
    public function update(Request $request, $id) {
        $r = $request->all();

        DB::beginTransaction();
        try {
            $medicine = Medicine::find($id);
            if($medicine) {
                $medicine->name = isset($r['name']) ? $r['name'] : $medicine->name;
                $medicine->generic_name = isset($r['generic_name']) ? $r['generic_name'] : $medicine->generic_name;
                $medicine->summary = isset($r['summary']) ? $r['summary'] : $medicine->summary;
                $medicine->description = isset($r['description']) ? $r['description'] : $medicine->description;
                $medicine->price = isset($r['price']) ? $r['price'] : $medicine->price;
                $medicine->category_id = isset($r['category_id']) ? $r['category_id'] : $medicine->category_id;
                $medicine->amount = isset($r['amount']) ? $r['amount'] : $medicine->amount;
                $medicine->unit = isset($r['unit']) ? $r['unit'] : $medicine->unit;
                $medicine->save();
    
                DB::commit();
                $response = [
                    'success' => true,
                    'messages' => 'Success!',
                    'data' => $medicine,
                ];
            } else {
                DB::rollBack();
                $response = [
                    'success' => false,
                    'messages' => 'Data not found!!',
                ];
            }
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Failed to create!',
            ];
        }
        return response()->json($response);
    }
    // delete id
    public function delete(Request $request, $id) {
        DB::beginTransaction();
        try {
            $medicine = Medicine::find($id);
            if($medicine) {
                $medicine->delete();
                DB::commit();
                $response = [
                    'success' => true,
                    'message' => 'Success deleted data!',
                ];
            } else {
                DB::rollBack();
                $response = [
                    'success' => false,
                    'message' => 'Data not found!',
                ];
            }
            
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'message' => 'Failed to delete!',
            ];
        }
        return response()->json($response);
    }
}