<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MedicineList;
use DB;

class MedicineListController extends Controller
{
    // return all items found
    public function index(Request $request) {
        $medicine_list = MedicineList::all();
        
        if(count($medicine_list) == 0) {
            $response = [
                'success' => false,
                'messages' => 'no data found',
            ];
        } elseif(count($medicine_list) == 1) {
            $response = [
                'success' => true,
                'messages' => count($medicine_list) . ' data found',
                'data' => $medicine_list,
            ];
        } else {
            $response = [
                'success' => true,
                'messages' => count($medicine_list) . ' datas found',
                'data' => $medicine_list,
            ];
        }
        
        return response()->json($response);
    }
    // view item by id
    public function view(Request $request, $id) {
        $medicine_list = MedicineList::find($id);

        if($medicine_list) {
            $response = [
                'success' => true,
                'messages' => 'Item found!',
                'data' => $medicine_list,
            ];
        } else {
            $response = [
                'success' => false,
                'messages' => 'Item not found!',
            ];
        }

        return response()->json($response);
    }

    // create new item
    public function create(Request $request) {
        $r = $request->all();

        DB::beginTransaction();
        try {    
            $medicine_list = new MedicineList;
        
            $medicine_list->name = isset($r['name']) ? $r['name'] : null;
            $medicine_list->doses = isset($r['doses']) ? $r['doses'] : null;
            $medicine_list->status = isset($r['status']) ? $r['status'] : null;
            $medicine_list->description = isset($r['description']) ? $r['description'] : null;
            $medicine_list->quantity = isset($r['quantity']) ? $r['quantity'] : null;
            $medicine_list->medicine_id = isset($r['medicine_id']) ? $r['medicine_id'] : null;
            $medicine_list->recipe_id = isset($r['recipe_id']) ? $r['recipe_id'] : null;
            $medicine_list->alarm_1 = isset($r['alarm_1']) ? $r['alarm_1'] : null;
            $medicine_list->alarm_2 = isset($r['alarm_2']) ? $r['alarm_2'] : null;
            $medicine_list->alarm_3 = isset($r['alarm_3']) ? $r['alarm_3'] : null;
            $medicine_list->alarm_desc_1 = isset($r['alarm__desc_1']) ? $r['alarm_desc_1'] : $medicine_list->null;
            $medicine_list->alarm_desc_2 = isset($r['alarm__desc_2']) ? $r['alarm_desc_2'] : null;
            $medicine_list->alarm_desc_3 = isset($r['alarm__desc_3']) ? $r['alarm_desc_3'] : null;
            $medicine_list->save();

            DB::commit();
            $response = [
                'success' => true,
                'messages' => 'Success!',
                'data' => [
                    $medicine_list,
                ],
            ];
        } catch(QueryException $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Failed to create',
            ];
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Oops! Something went wrong.',
            ];
        }
        return response()->json($response);
    }
    // update item by id
    public function update(Request $request, $id) {
        $r = $request->all();

        DB::beginTransaction();
        try {    
            $medicine_list = MedicineList::find($id);
            if($medicine_list) {
                $medicine_list->name = isset($r['name']) ? $r['name'] : $medicine_list->name;
                $medicine_list->doses = isset($r['doses']) ? $r['doses'] : $medicine_list->doses;
                $medicine_list->status = isset($r['status']) ? $r['status'] : $medicine_list->status;
                $medicine_list->description = isset($r['description']) ? $r['description'] : $medicine_list->description;
                $medicine_list->quantity = isset($r['quantity']) ? $r['quantity'] : $medicine_list->quantity;
                $medicine_list->medicine_id = isset($r['medicine_id']) ? $r['medicine_id'] : $medicine_list->medicine_id;
                $medicine_list->recipe_id = isset($r['recipe_id']) ? $r['recipe_id'] : $medicine_list->recipe;
                $medicine_list->alarm_1 = isset($r['alarm_1']) ? $r['alarm_1'] : $medicine_list->alarm_1;
                $medicine_list->alarm_2 = isset($r['alarm_2']) ? $r['alarm_2'] : $medicine_list->alarm_2;
                $medicine_list->alarm_3 = isset($r['alarm_3']) ? $r['alarm_3'] : $medicine_list->alarm_3;
                $medicine_list->alarm_desc_1 = isset($r['alarm__desc_1']) ? $r['alarm_desc_1'] : $medicine_list->alarm_desc_1;
                $medicine_list->alarm_desc_2 = isset($r['alarm__desc_2']) ? $r['alarm_desc_2'] : $medicine_list->alarm_desc_2;
                $medicine_list->alarm_desc_3 = isset($r['alarm__desc_3']) ? $r['alarm_desc_3'] : $medicine_list->alarm_desc_3;
                $medicine_list->save();
                
                DB::commit();
                $response = [
                    'success' => true,
                    'messages' => 'Success!',
                    'data' => $medicine_list,
                ];
            } else {
                DB::rollBack();
                $response = [
                    'success' => false,
                    'messages' => 'Data not found!',
                ];
            }
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Failed to update!',
            ];
        }
        return response()->json($response);
    }
    // delete id
    public function delete(Request $request, $id) {
        DB::beginTransaction();
        try {
            $medicine_list = MedicineList::find($id);
            if($medicine_list) {
                $medicine_list->delete();
                DB::commit();
                $response = [
                    'success' => true,
                    'message' => 'Success deleted data!',
                ];
            } else {
                DB::rollBack();
                $response = [
                    'success' => false,
                    'message' => 'Data not found!',
                ];
            }
            
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'message' => 'Failed to delete!',
            ];
        }
        return response()->json($response);
    }
}