<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Doctor;
use DB;

class DoctorController extends Controller
{
    // return all items found
    public function index(Request $request) {
        $doctor = Doctor::all();
        
        if(count($doctor) == 0) {
            $response = [
                'success' => false,
                'messages' => 'no data found',
            ];
        } elseif(count($doctor) == 1) {
            $response = [
                'success' => true,
                'messages' => count($doctor) . ' data found',
                'data' => $doctor,
            ];
        } else {
            $response = [
                'success' => true,
                'messages' => count($doctor) . ' datas found',
                'data' => $doctor,
            ];
        }
        
        return response()->json($response);
    }
    // view item by id
    public function view(Request $request, $id) {
        $doctor = Doctor::find($id);

        if($doctor) {
            $response = [
                'success' => true,
                'messages' => 'Data found!',
                'data' => $doctor,
            ];
        } else {
            $response = [
                'success' => false,
                'messages' => 'Data not found!',
            ];
        }

        return response()->json($response);
    }
    // create new item
    public function create(Request $request) {
        $r = $request->all();
        
        DB::beginTransaction();
        try {
            $doctor = new Doctor;
            $doctor->name = isset($r['name']) ? $r['name'] : null;
            $doctor->email = isset($r['email']) ? $r['email'] : null;
            $doctor->phone_number = isset($r['phone_number']) ? $r['phone_number'] : null;
            $doctor->specialist = isset($r['specialist']) ? $r['specialist'] : null;
            $doctor->save();

            DB::commit();
            $response = [
                'success' => true,
                'messages' => 'Success!',
                'data' => $doctor,
            ];
        } catch(QueryException $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Failed to create',
            ];
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Oops! Something went wrong.',
            ];
        }
        return response()->json($response);
    }
    // update item by id
    public function update(Request $request, $id) {
        $r = $request->all();

        DB::beginTransaction();
        try {
            $doctor = Doctor::find($id);
            if($doctor) {
                $doctor->name = isset($r['name']) ? $r['name'] : $doctor->name;
                $doctor->email = isset($r['email']) ? $r['email'] : $doctor->email;
                $doctor->phone_number = isset($r['phone_number']) ? $r['phone_number'] : $doctor->phone_number;
                $doctor->specialist = isset($r['specialist']) ? $r['specialist'] : $doctor->specialist;
                $doctor->save();
    
                DB::commit();
                $response = [
                    'success' => true,
                    'messages' => 'Success!',
                    'data' => $doctor,
                ];
            } else {
                DB::rollBack();
                $response = [
                    'success' => false,
                    'messages' => 'Data not found!!',
                ];
            }
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Failed to create!',
            ];
        }
        return response()->json($response);
    }
    // delete id
    public function delete(Request $request, $id) {
        DB::beginTransaction();
        try {
            $doctor = Doctor::find($id);
            if($doctor) {
                $doctor->delete();
                DB::commit();
                $response = [
                    'success' => true,
                    'message' => 'Success deleted data!',
                ];
            } else {
                DB::rollBack();
                $response = [
                    'success' => false,
                    'message' => 'Data not found!',
                ];
            }
            
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'message' => 'Failed to delete!',
            ];
        }
        return response()->json($response);
    }
}