<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Alarm;
use DB;

class AlarmController extends Controller
{
    // return all items found
    public function index(Request $request) {
        $alarm = Alarm::all();
        
        if(count($alarm) == 0) {
            $response = [
                'success' => false,
                'messages' => 'no data found',
            ];
        } elseif(count($alarm) == 1) {
            $response = [
                'success' => true,
                'messages' => count($alarm) . ' data found',
                'data' => $alarm,
            ];
        } else {
            $response = [
                'success' => true,
                'messages' => count($alarm) . ' datas found',
                'data' => $alarm,
            ];
        }
        
        return response()->json($response);
    }
    // view item by id
    public function view(Request $request, $id) {
        $alarm = Alarm::find($id);

        if($alarm) {
            $response = [
                'success' => true,
                'messages' => 'Data found!',
                'data' => $alarm,
            ];
        } else {
            $response = [
                'success' => false,
                'messages' => 'Data not found!',
            ];
        }

        return response()->json($response);
    }
    // create new item
    public function create(Request $request) {
        $r = $request->all();
        
        DB::beginTransaction();
        try {
            $alarm = new Alarm;
            $alarm->time = isset($r['time']) ? $r['time'] : null;
            $alarm->category = isset($r['category']) ? $r['category'] : null;
            $alarm->save();

            DB::commit();
            $response = [
                'success' => true,
                'messages' => 'Success!',
                'data' => $alarm,
            ];
        } catch(QueryException $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Failed to create',
            ];
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Oops! Something went wrong.',
            ];
        }
        return response()->json($response);
    }
    // update item by id
    public function update(Request $request, $id) {
        $r = $request->all();
        
        DB::beginTransaction();
        try {
            $alarm = Alarm::find($id);
            if($alarm) {
                $alarm->name = isset($r['name']) ? $r['name'] : $alarm->name;
                $alarm->usage = isset($r['usage']) ? $r['usage'] : $alarm->usage;
                $alarm->prohibition = isset($r['prohibition']) ? $r['prohibition'] : $alarm->prohibiton;
                $alarm->info = isset($r['info']) ? $r['info'] : $alarm->info;
                $alarm->save();
    
                DB::commit();
                $response = [
                    'success' => true,
                    'messages' => 'Success!',
                    'data' => $alarm,
                ];
            } else {
                DB::rollBack();
                $response = [
                    'success' => false,
                    'messages' => 'Data not found!!',
                ];
            }
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Failed to create!',
            ];
        }
        return response()->json($response);
    }
    // delete id
    public function delete(Request $request, $id) {
        DB::beginTransaction();
        try {
            $alarm = Alarm::find($id);
            if($alarm) {
                $alarm->delete();
                DB::commit();
                $response = [
                    'success' => true,
                    'message' => 'Success deleted data!',
                ];
            } else {
                DB::rollBack();
                $response = [
                    'success' => false,
                    'message' => 'Data not found!',
                ];
            }
            
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'message' => 'Failed to delete!',
            ];
        }
        return response()->json($response);
    }
}