<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Patient;
use DB;

class PatientController extends Controller
{
    // return all items found
    public function index(Request $request) {
        $patient = Patient::all();
        
        if(count($patient) == 0) {
            $response = [
                'success' => false,
                'messages' => 'no data found',
            ];
        } elseif(count($patient) == 1) {
            $response = [
                'success' => true,
                'messages' => count($patient) . ' data found',
                'data' => $patient,
            ];
        } else {
            $response = [
                'success' => true,
                'messages' => count($patient) . ' datas found',
                'data' => $patient,
            ];
        }
        
        return response()->json($response);
    }
    // view item by id
    public function view(Request $request, $id) {
        $patient = Patient::find($id);

        if($patient) {
            $response = [
                'success' => true,
                'messages' => 'Data found!',
                'data' => $patient,
            ];
        } else {
            $response = [
                'success' => false,
                'messages' => 'Data not found!',
            ];
        }

        return response()->json($response);
    }
    // create new item
    public function create(Request $request) {
        $r = $request->all();
        
        DB::beginTransaction();
        try {
            $patient = new Patient;
            $patient->name = isset($r['name']) ? $r['name'] : null;
            $patient->email = isset($r['email']) ? $r['email'] : null;
            $patient->phone_number = isset($r['phone_number']) ? $r['phone_number'] : null;
            $patient->address = isset($r['address']) ? $r['address'] : null;
            $patient->birth_date = isset($r['birth_date']) ? $r['birth_date'] : null;
            $patient->save();

            DB::commit();
            $response = [
                'success' => true,
                'messages' => 'Success!',
                'data' => $patient,
            ];
        } catch(QueryException $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Failed to create',
            ];
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Oops! Something went wrong.',
            ];
        }
        return response()->json($response);
    }
    // update item by id
    public function update(Request $request, $id) {
        $r = $request->all();

        DB::beginTransaction();
        try {
            $patient = Patient::find($id);
            if($patient) {
                $patient->name = isset($r['name']) ? $r['name'] : $patient->name;
                $patient->email = isset($r['email']) ? $r['email'] : $patient->email;
                $patient->phone_number = isset($r['phone_number']) ? $r['phone_number'] : $patient->phone_number;
                $patient->address = isset($r['address']) ? $r['address'] : $patient->address;
                $patient->birth_date = isset($r['birth_date']) ? $r['birth_date'] : $patient->birth_date;
                $patient->save();
    
                DB::commit();
                $response = [
                    'success' => true,
                    'messages' => 'Success!',
                    'data' => $patient,
                ];
            } else {
                DB::rollBack();
                $response = [
                    'success' => false,
                    'messages' => 'Data not found!!',
                ];
            }
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Failed to create!',
            ];
        }
        return response()->json($response);
    }
    // delete id
    public function delete(Request $request, $id) {
        DB::beginTransaction();
        try {
            $patient = Patient::find($id);
            if($patient) {
                $patient->delete();
                DB::commit();
                $response = [
                    'success' => true,
                    'message' => 'Success deleted data!',
                ];
            } else {
                DB::rollBack();
                $response = [
                    'success' => false,
                    'message' => 'Data not found!',
                ];
            }
            
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'message' => 'Failed to delete!',
            ];
        }
        return response()->json($response);
    }
}