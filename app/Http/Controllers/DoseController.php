<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dose;
use DB;

class DoseController extends Controller
{
    // return all items found
    public function index(Request $request) {
        $dose = Dose::all();
        
        if(count($dose) == 0) {
            $response = [
                'success' => false,
                'messages' => 'no data found',
            ];
        } elseif(count($dose) == 1) {
            $response = [
                'success' => true,
                'messages' => count($dose) . ' data found',
                'data' => $dose,
            ];
        } else {
            $response = [
                'success' => true,
                'messages' => count($dose) . ' datas found',
                'data' => $dose,
            ];
        }
        
        return response()->json($response);
    }
    // view item by id
    public function view(Request $request, $id) {
        $dose = Dose::find($id);

        if($dose) {
            $response = [
                'success' => true,
                'messages' => 'Data found!',
                'data' => $dose,
            ];
        } else {
            $response = [
                'success' => false,
                'messages' => 'Data not found!',
            ];
        }

        return response()->json($response);
    }
    // create new item
    public function create(Request $request) {
        $r = $request->all();
        
        DB::beginTransaction();
        try {
            $dose = new Dose;
            $dose->min_dose = isset($r['min_dose']) ? $r['min_dose'] : null;
            $dose->max_dose = isset($r['max_dose']) ? $r['max_dose'] : null;
            $dose->unit = isset($r['unit']) ? $r['unit'] : null;
            $dose->min_age = isset($r['min_age']) ? $r['min_age'] : null;
            $dose->max_age = isset($r['max_age']) ? $r['max_age'] : null;
            $dose->time_1 = isset($r['time_1']) ? $r['time_1'] : null;
            $dose->time_unit_1 = isset($r['time_unit_1']) ? $r['time_unit_1'] : null;
            $dose->time_2 = isset($r['time_2']) ? $r['time_2'] : null;
            $dose->time_unit_2 = isset($r['time_unit_2']) ? $r['time_unit_2'] : null;
            $dose->category_id = isset($r['category_id']) ? $r['category_id'] : null;
            $dose->save();

            DB::commit();
            $response = [
                'success' => true,
                'messages' => 'Success!',
                'data' => $dose,
            ];
        } catch(QueryException $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Failed to create',
            ];
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Oops! Something went wrong.',
            ];
        }
        return response()->json($response);
    }
    // update item by id
    public function update(Request $request, $id) {
        $r = $request->all();
        
        DB::beginTransaction();
        try {
            $dose = Dose::find($id);
            if($dose) {
                $dose->min_dose = isset($r['min_dose']) ? $r['min_dose'] : $dose->min_dose;
                $dose->max_dose = isset($r['max_dose']) ? $r['max_dose'] : $dose->max_dose;
                $dose->unit = isset($r['unit']) ? $r['unit'] : $dose->unit;
                $dose->min_age = isset($r['min_age']) ? $r['min_age'] : $dose->min_age;
                $dose->max_age = isset($r['max_age']) ? $r['max_age'] : $dose->max_age;
                $dose->time_1 = isset($r['time_1']) ? $r['time_1'] : $dose->time_1;
                $dose->time_unit_1 = isset($r['time_unit_1']) ? $r['time_unit_1'] : $dose->time_unit_1;
                $dose->time_2 = isset($r['time_2']) ? $r['time_2'] : $dose->time_2;
                $dose->time_unit_2 = isset($r['time_unit_2']) ? $r['time_unit_2'] : $dose->time_unit_2;
                $dose->category_id = isset($r['category_id']) ? $r['category_id'] : $dose->category_id;
                $dose->save();
    
                DB::commit();
                $response = [
                    'success' => true,
                    'messages' => 'Success!',
                    'data' => $dose,
                ];
            } else {
                DB::rollBack();
                $response = [
                    'success' => false,
                    'messages' => 'Data not found!!',
                ];
            }
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Failed to create!',
            ];
        }
        return response()->json($response);
    }
    // delete id
    public function delete(Request $request, $id) {
        DB::beginTransaction();
        try {
            $dose = Dose::find($id);
            if($dose) {
                $dose->delete();
                DB::commit();
                $response = [
                    'success' => true,
                    'message' => 'Success deleted data!',
                ];
            } else {
                DB::rollBack();
                $response = [
                    'success' => false,
                    'message' => 'Data not found!',
                ];
            }
            
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'message' => 'Failed to delete!',
            ];
        }
        return response()->json($response);
    }
}