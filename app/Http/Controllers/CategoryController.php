<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use DB;

class CategoryController extends Controller
{
    // return all items found
    public function index(Request $request) {
        $category = Category::all();
        
        if(count($category) == 0) {
            $response = [
                'success' => false,
                'messages' => 'no data found',
            ];
        } elseif(count($category) == 1) {
            $response = [
                'success' => true,
                'messages' => count($category) . ' data found',
                'data' => $category,
            ];
        } else {
            $response = [
                'success' => true,
                'messages' => count($category) . ' datas found',
                'data' => $category,
            ];
        }
        
        return response()->json($response);
    }
    // view item by id
    public function view(Request $request, $id) {
        $category = Category::find($id);

        if($category) {
            $response = [
                'success' => true,
                'messages' => 'Data found!',
                'data' => $category,
            ];
        } else {
            $response = [
                'success' => false,
                'messages' => 'Data not found!',
            ];
        }

        return response()->json($response);
    }
    // create new item
    public function create(Request $request) {
        $r = $request->all();
        
        DB::beginTransaction();
        try {
            $category = new Category;
            $category->name = isset($r['name']) ? $r['name'] : null;
            $category->usage = isset($r['usage']) ? $r['usage'] : null;
            $category->prohibition = isset($r['prohibition']) ? $r['prohibition'] : null;
            $category->info = isset($r['info']) ? $r['info'] : null;
            $category->save();

            DB::commit();
            $response = [
                'success' => true,
                'messages' => 'Success!',
                'data' => $category,
            ];
        } catch(QueryException $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Failed to create',
            ];
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Oops! Something went wrong.',
            ];
        }
        return response()->json($response);
    }
    // update item by id
    public function update(Request $request, $id) {
        $r = $request->all();
        
        DB::beginTransaction();
        try {
            $category = Category::find($id);
            if($category) {
                $category->name = isset($r['name']) ? $r['name'] : $category->name;
                $category->usage = isset($r['usage']) ? $r['usage'] : $category->usage;
                $category->prohibition = isset($r['prohibition']) ? $r['prohibition'] : $category->prohibiton;
                $category->info = isset($r['info']) ? $r['info'] : $category->info;
                $category->save();
    
                DB::commit();
                $response = [
                    'success' => true,
                    'messages' => 'Success!',
                    'data' => $category,
                ];
            } else {
                DB::rollBack();
                $response = [
                    'success' => false,
                    'messages' => 'Data not found!!',
                ];
            }
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Failed to create!',
            ];
        }
        return response()->json($response);
    }
    // delete id
    public function delete(Request $request, $id) {
        DB::beginTransaction();
        try {
            $category = Category::find($id);
            if($category) {
                $category->delete();
                DB::commit();
                $response = [
                    'success' => true,
                    'message' => 'Success deleted data!',
                ];
            } else {
                DB::rollBack();
                $response = [
                    'success' => false,
                    'message' => 'Data not found!',
                ];
            }
            
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'message' => 'Failed to delete!',
            ];
        }
        return response()->json($response);
    }
}