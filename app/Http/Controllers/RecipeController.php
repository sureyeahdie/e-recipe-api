<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Recipe;
use App\MedicineList;
use DB;

class RecipeController extends Controller
{
    // return all items found
    public function index(Request $request) {
        $recipe = Recipe::all();
        
        if(count($recipe) == 0) {
            $response = [
                'success' => false,
                'messages' => 'no data found',
            ];
        } elseif(count($recipe) == 1) {
            $response = [
                'success' => true,
                'messages' => count($recipe) . ' data found',
                'data' => $recipe,
            ];
        } else {
            $response = [
                'success' => true,
                'messages' => count($recipe) . ' datas found',
                'data' => $recipe,
            ];
        }
        
        return response()->json($response);
    }
    // view item by id
    public function view(Request $request, $id) {
        $recipe = Recipe::find($id);

        if($recipe) {
            $response = [
                'success' => true,
                'messages' => 'Item found!',
                'data' => $recipe,
            ];
        } else {
            $response = [
                'success' => false,
                'messages' => 'Item not found!',
            ];
        }

        return response()->json($response);
    }
    // view medicine list
    public function viewMedList($id) {
        return Recipe::find($id)->medicine_lists;
    }
    // create new item
    public function create(Request $request) {
        $r = $request->all();
        $med_arr = array();
        // if(isset($r['medicine'])) {
        //     foreach(json_decode($r['medicine']) as $med) {
        //         array_push($arr, $med);
        //     }    
        // }
        // return $r['medicine'];
        DB::beginTransaction();
        try {    
            $recipe = new Recipe;
            
            $recipe->doctor_id = isset($r['doctor_id']) ? $r['doctor_id'] : null;
            $recipe->patient_id = isset($r['patient_id']) ? $r['patient_id'] : null;
            $recipe->pharmacist_id = isset($r['pharmacist_id']) ? $r['pharmacist_id'] : null;
            $recipe->description = isset($r['description']) ? $r['description'] : null;
            // $recipe->clinic_id = isset($r['clinic_id']) ? $r['clinic_id'] : null;
            $recipe->save();

            if(isset($r['medicine'])) {
                foreach(json_decode($r['medicine']) as $med) {
                    array_push($med_arr, $med);
                    $med_list = new MedicineList;
                    $med_list->name = $med->name;
                    $med_list->doses = $med->doses;
                    $med_list->recipe_id = $recipe->id;
                    $med_list->save();
                }    
            }

            DB::commit();
            $response = [
                'success' => true,
                'messages' => 'Success!',
                'data' => [
                    $recipe,
                    $med_arr,
                ],
            ];
        } catch(QueryException $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Failed to create',
            ];
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Oops! Something went wrong.',
            ];
        }
        return response()->json($response);
    }
    // update item by id
    public function update(Request $request, $id) {
        $r = $request->all();

        DB::beginTransaction();
        try {    
            $recipe = Recipe::find($id);
            if($recipe) {
                // $recipe->recipe_no = isset($r['recipe_no']) ? $r['recipe_no'] : $recipe->recipe_no;
                // $recipe->doses = isset($r['doses']) ? $r['doses'] : $recipe->doses;
                // $recipe->signature = isset($r['signature']) ? $r['signature'] : $recipe->signature;
                // $recipe->quantity = isset($r['quantity']) ? $r['quantity'] : $recipe->quantity;
                // $recipe->medicine_id = isset($r['medicine_id']) ? $r['medicine_id'] : $recipe->medicine_id;
                // $recipe->doctor_id = isset($r['doctor_id']) ? $r['doctor_id'] : $recipe->doctor_id;
                // $recipe->patient_id = isset($r['patient_id']) ? $r['patient_id'] : $recipe->patient_id;
                // $recipe->clinic_id = isset($r['clinic_id']) ? $r['clinic_id'] : $recipe->clinic_id;
                $recipe->doctor_id = isset($r['doctor_id']) ? $r['doctor_id'] : $recipe->doctor_id;
                $recipe->patient_id = isset($r['patient_id']) ? $r['patient_id'] : $recipe->patient_id;
                $recipe->pharmacist_id = isset($r['pharmacist_id']) ? $r['pharmacist_id'] : $recipe->pharmacist_id;
                $recipe->description = isset($r['description']) ? $r['description'] : $recipe->description;
                $recipe->save();
                
                DB::commit();
                $response = [
                    'success' => true,
                    'messages' => 'Success!',
                    'data' => $recipe,
                ];
            } else {
                DB::rollBack();
                $response = [
                    'success' => false,
                    'messages' => 'Data not found!',
                ];
            }
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Failed to update!',
            ];
        }
        return response()->json($response);
    }
    // delete id
    public function delete(Request $request, $id) {
        DB::beginTransaction();
        try {
            $recipe = Recipe::find($id);
            if($recipe) {
                $recipe->delete();
                DB::commit();
                $response = [
                    'success' => true,
                    'message' => 'Success deleted data!',
                ];
            } else {
                DB::rollBack();
                $response = [
                    'success' => false,
                    'message' => 'Data not found!',
                ];
            }
            
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'message' => 'Failed to delete!',
            ];
        }
        return response()->json($response);
    }
}