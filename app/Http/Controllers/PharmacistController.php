<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pharmacist;
use DB;

class PharmacistController extends Controller
{
    // return all items found
    public function index(Request $request) {
        $pharmacist = Pharmacist::all();
        
        if(count($pharmacist) == 0) {
            $response = [
                'success' => false,
                'messages' => 'no data found',
            ];
        } elseif(count($pharmacist) == 1) {
            $response = [
                'success' => true,
                'messages' => count($pharmacist) . ' data found',
                'data' => $pharmacist,
            ];
        } else {
            $response = [
                'success' => true,
                'messages' => count($pharmacist) . ' datas found',
                'data' => $pharmacist,
            ];
        }
        
        return response()->json($response);
    }
    // view item by id
    public function view(Request $request, $id) {
        $pharmacist = Pharmacist::find($id);

        if($pharmacist) {
            $response = [
                'success' => true,
                'messages' => 'Data found!',
                'data' => $pharmacist,
            ];
        } else {
            $response = [
                'success' => false,
                'messages' => 'Data not found!',
            ];
        }

        return response()->json($response);
    }
    // create new item
    public function create(Request $request) {
        $r = $request->all();
        
        DB::beginTransaction();
        try {
            $pharmacist = new Pharmacist;
            $pharmacist->name = isset($r['name']) ? $r['name'] : null;
            $pharmacist->email = isset($r['email']) ? $r['email'] : null;
            $pharmacist->phone_number = isset($r['phone_number']) ? $r['phone_number'] : null;
            $pharmacist->sik = isset($r['sik']) ? $r['sik'] : null;
            $pharmacist->save();

            DB::commit();
            $response = [
                'success' => true,
                'messages' => 'Success!',
                'data' => $pharmacist,
            ];
        } catch(QueryException $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Failed to create',
            ];
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Oops! Something went wrong.',
            ];
        }
        return response()->json($response);
    }
    // update item by id
    public function update(Request $request, $id) {
        $r = $request->all();

        DB::beginTransaction();
        try {
            $pharmacist = Pharmacist::find($id);
            if($pharmacist) {
                $pharmacist->name = isset($r['name']) ? $r['name'] : $pharmacist->name;
                $pharmacist->email = isset($r['email']) ? $r['email'] : $pharmacist->email;
                $pharmacist->phone_number = isset($r['phone_number']) ? $r['phone_number'] : $pharmacist->phone_number;
                $pharmacist->sik =  isset($r['sik']) ? $r['sik'] : $pharmacist->sik;
                $pharmacist->save();
    
                DB::commit();
                $response = [
                    'success' => true,
                    'messages' => 'Success!',
                    'data' => $pharmacist,
                ];
            } else {
                DB::rollBack();
                $response = [
                    'success' => false,
                    'messages' => 'Data not found!!',
                ];
            }
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'messages' => 'Failed to create!',
            ];
        }
        return response()->json($response);
    }
    // delete id
    public function delete(Request $request, $id) {
        DB::beginTransaction();
        try {
            $pharmacist = Pharmacist::find($id);
            if($pharmacist) {
                $pharmacist->delete();
                DB::commit();
                $response = [
                    'success' => true,
                    'message' => 'Success deleted data!',
                ];
            } else {
                DB::rollBack();
                $response = [
                    'success' => false,
                    'message' => 'Data not found!',
                ];
            }
            
        } catch(Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'message' => 'Failed to delete!',
            ];
        }
        return response()->json($response);
    }
}